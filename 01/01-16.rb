# 繰り返し処理
# times
# while

# 3.times do |i|
# 	puts "#{i}回目のhello"
# end

=begin
i = 0
while i < 3 do
	puts "#{i} hello"
	i += 1
end
=end

#break:ループを抜ける
#next:ループを一回スキップ

3.times do |i|
	if i == 1
		#break
		next
	end
	puts "#{i}hello"
end