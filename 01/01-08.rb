#配列オブジェクト
# sales_1 sales_2 ...

sales = [5,8,4]
#sales[1]=10
#p sales[1]

# p sales[0..2]
# p sales[0...2]#未満
p sales[-1] #最後の要素を見る
p sales[1,2]#要素1,2を見る
