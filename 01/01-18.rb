#関数的メソッド

def sayHi(name = "Steve")
	# puts "hello,#{name}"
	s = "hello," + name
	return s
end

# sayHi("bob")
# sayHi("Tom")
# sayHi()

greet = sayHi("bob")
puts greet