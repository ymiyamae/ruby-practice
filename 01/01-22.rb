#アクセサ

class User
	attr_accessor :name#getter + setter
	attr_reader :name #getter
	attr_writer :name #getter

	def initialize(name)
		@name = name
	end

	# def name #getter
	# 	@name
	# end

	# def setName(newName) #setter
	# 	@name = newName
	# end

	def sayHi
		puts "hello,my name is #{@name}"
	end
end

bob = User.new('bob')
bob.sayHi()
p bob.name
bob.name = 'tom'
bob.sayHi()