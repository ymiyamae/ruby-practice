a = 10
b = "5"

p a + b.to_i #to_iは整数に直す
p a + b.to_f #to_fは実数に直す
p a.to_s + b #to_sは文字列に直す

h = {taguchi: 100,fkoji: 200}
p h.to_a#to_aは配列に直す
p h.to_a.to_h#to_hはハッシュに直す