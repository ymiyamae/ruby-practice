# クラス（オブジェクトの設計図）
# メソッド
# インスタンス　←クラスから作られたオブジェクト	


class User
	def initialize(name)
		@name = name #インスタンス変数を使えば、別のメソッドから呼び出せる
	end

	def sayHi
		puts "hello,my name is #{@name}"
	end
end

tom = User.new("Tom")
tom.sayHi()
