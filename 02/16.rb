# 繰り返し処理
# times
# while 

=begin
3.times do |i|
	puts "hello#{i}回目"
end

# break:　ループを抜ける
# next : ループを一回スキップ

i = 0
while i<3 do
	puts "#{i};hello"
	i += 1
end

=end


3.times do |i|
	if i==1
		# break; #0のみ
		next; #1のみ表示されない
	end
	puts "#{i}:hello"
end

