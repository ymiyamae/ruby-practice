# クラス(オブジェクトの設計図)
# メソッド
# インスタンス

class User

	def initialize(name)
		@name = name#←この値は引数
	end

	def sayHi
		puts "hello,#{@name}"
	end
end

tom = User.new("tom")
tom.sayHi()

