#　%記法

# s = "hello"
# s = %Q(he"llo)#文字列
s = %(he"llo)#Qは省略可能

# puts s

#a = ["a","b","c"]
a = %w(a b c)
a = ['a','b','c']
a = %W(A B C)

puts a
