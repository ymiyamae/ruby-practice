#数値オブジェクト-Numeric class

x = 10 #10_00_00このようにもかける
y = 20.5
z = 1/3r #Rational(1,3)

# p x % 3 #剰余
# p x ** 3 #べき乗
# p z * 2 #2/3

x += 5 # x = x + 5
p x #15

p y.round#四捨五入