# 配列オブジェックト
# sales_1 sales_2 ...

sales = [5,8,4]
# p sales[1] # 8

# sales[1] = 10 # 8→10

# p sales[0..2]#０から２以下
# p sales[0...2]#０から２未満

p sales[-1]
p sales[1,2]#１番目の要素から２個分とってくる [8,4]