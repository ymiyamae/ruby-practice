# 配列オブジェクト

sales = [5,8,4]

#sales[0...2]=[1,2] #[1,2,4]
# p sales
# sales[1,2] = [10,11,12]#１番目の要素から０個のところを変更する


p sales.size#要素数
p sales.sort#小さい順
p sales.reverse#逆にする
p sales.push(100)#末尾に追加
sales << 100
p sales