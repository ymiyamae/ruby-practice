#アクセサ

class User

	attr_accessor :name #getter,setter備える
	# attr_reader :name #getter
	# attr_writer :name #setter

	def initialize(name)
		@name = name
	end
=begin
	#setter
	def name
		return @name
	end

	#getter
	def setName(newName)
		return @name = newName
	end
=end

	def sayHi
		puts "hello #{@name}"
	end
end

bob = User.new("BOB")
bob.sayHi()
p bob.name #getter
bob.name = "tom" #setter
bob.sayHi()
