# 繰り返し処理
# for
# each

# for i in 0..2 do
# 	puts i
# end

# for color in ["red","blue","pink"] do
# 	puts color
# end

# color = ["red","blue","pink"]
# color.each do |colors|
# 	puts colors
# end

{red: 100,blue: 200,pink: 80}.each do |color,price|
	puts "#{color}:#{price}"
end
