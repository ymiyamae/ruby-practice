#文字列オブジェクト

name = "taguchi"

x = "he\tllo\n"#{name} #　変数展開、特殊文字
y = 'hel\tlo\n#{name}' # #{name}がそのまま表示 

#puts x
#puts y

# + *
puts "hello" + "taguchi"
puts "hello" * 5
