#クラス（オブジェクトの設計図）

class User

	attr_accessor :name
	@@count = 0

	def initialize(name)
		@name = name
		@@count += 1
	end

	def sayHi
		"hello #{@name}"
	end

	def User.sayHello
		"hello,my name is #{@name}(#{@@count}user)"
	end
end


tom = User.new("tom")
puts tom.sayHi
bob = User.new("tom") 
puts User.sayHello
#クラスメソッドでインスタンス変数を渡すにはどうしたらいいか？
