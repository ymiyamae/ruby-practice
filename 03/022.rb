#アクセサ

class User
	attr_accessor :name
	# attr_reader :name
	# attr_writer :name

	def initialize(name)
		@name = name
	end

	def sayHi
		"hello #{@name}"
	end

	# def name
	# 	@name
	# end
	# def setName
	# 	@name = newName
	# end
end

bob = User.new("bob")
puts bob.sayHi()
tom = User.new("tom")
tom.name = "Tom"
puts tom.sayHi