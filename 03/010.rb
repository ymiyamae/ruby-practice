#ハッシュ
#key value 

# sales = {"taguchi"=>100,"koji"=>300}
# p sales["taguchi"]

# sales = {:taguchi =>20,:koji=>200}
 sales = {taguchi: 20,koji: 200}
p sales[:taguchi]

p sales.size
p sales.keys
p sales.values
p sales.has_key?(:taguchi)