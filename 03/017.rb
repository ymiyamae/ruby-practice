#繰り返し
#for
#each

=begin
for i in 0..2 do
	puts i
end

=end

# for color in ["red","blue","pink"] do
# 	puts color
# end

# ["red","blue","pink"].each do |color|
# 	puts color
# end

colors = {red:100,blue:30,pink:44}

colors.each do|color,price|
	puts "#{color},#{price}"
end