# 配列オブジェクト
# sales_1 sales_2 ...

sales=[5,8,4]

# p sales[1] #１番目の要素を取ってくる
# sales[1] = 10 #書き換え

p sales[0..2] # 0~2以下
p sales[0...2]#0~2未満

p sales[-1] #最後の要素
p sales [0,2] #０要素から２個分