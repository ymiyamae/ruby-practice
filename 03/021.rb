#クラス（オブジェクトの設計図）

class User

	def initialize(name)
		@name = name
	end

	def sayHi
		"hello #{@name}"
	end
end

class SuperUser < User
	def shout
		"HELLO from #{@name}"
	end
end

tom = User.new("tom")
bob = SuperUser.new("bob")
puts tom.sayHi
puts bob.shout