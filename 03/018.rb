#関数的メソッド

def sayHi(name = "steve")
	# puts "hello:#{name}"
	s = "hello:#{name}"
	return s
end


# sayHi("tom")
# sayHi("bob")
# sayHi()

greet = sayHi()
puts greet
