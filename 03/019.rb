#クラス(オブジェクトの設計図)
#メソッド
#インスタンス:クラスから呼ばれたオブジェクト

class User
	def initialize(name)#クラスからインスタンスを作るときの初期化処理
		@name = name
	end

	def sayHi
		 "hello #{@name}"
	end
end

tom = User.new("tom")
bob = User.new("bob")

puts tom.sayHi
puts bob.sayHi

