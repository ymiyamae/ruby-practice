#繰り返し処理
#times while 
=begin
3.times do |i|
	puts "hello#{i}"
end


i= 0

while i<3
	puts "hello#{i}"
	i += 1
end

=end
#break
#next

3.times do |i|
	if i == 1
		# break
		next
	end
	puts "#{i}hello"
end