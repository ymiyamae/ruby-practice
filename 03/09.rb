#配列オブジェックト
sales = [5,8,4]
# sales = [0...2] = [1,2]#０から２未満を1,2に帰る

# sales[1,0] = [10,11,22]

# sales[0,2] = []　０から２個分を削除
# p sales

p sales.size #要素数
p sales.sort #照準
p sales.reverse #逆
p sales.push(100)#末尾に加える

sales << 100

p sales